﻿$(document).ready(function () {


    $('a img').animate({ opacity: .3 });
    $('a img').hover(function () {
        $(this).stop().animate({ opacity: 1 });
    }, function () {
        $(this).stop().animate({ opacity: .3 });
    });

    jQuery("a[rel^='prettyPhoto'], a[rel^='lightbox']").prettyPhoto({
        overlay_gallery: false, social_tools: '', 'theme': 'facebook' /* light_square / dark_rounded / light_square / dark_square / facebook */
    });
});